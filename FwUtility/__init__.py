import pyximport; pyximport.install()
try:
    from .misc import *
except:
    from misc import *
    
try:
    from .nums import *
except:
    from nums import *

try:
    from .parallel_processing import *
except ImportError:
    from parallel_processing import *

__version__ = "1.1.3"