﻿from math import log10
from operator import itemgetter

__all__ = ["abs_min", "abs_max",
           "count_range", "ordinal", "round_count",]

def abs_min(*values):
    if isinstance(values, tuple): values = list(values)
    if not values:
        return None
    elif len(values) == 2:
        return abs_min_ints(values[0], values[1])
    elif len(values) > 2:
        return abs_min_list(values)
    elif len(values) == 1 and isinstance(values[0], (list, tuple)):
        return abs_min_list(list(values[0]))
    else:
        return values[0]


cdef int abs_min_ints(int i, int j):
    return j if abs(j) < abs(i) else i


cdef int abs_min_list(list values):
    return min(values, key=lambda x: abs(x))


def abs_max(*values):
    if isinstance(values, tuple): values = list(values)
    if not values:
        return None
    elif len(values) == 2:
        return abs_max_ints(values[0], values[1])
    elif len(values) > 2:
        return abs_max_list(values)
    elif len(values) == 1 and isinstance(values[0], (list, tuple)):
        return abs_max_list(list(values[0]))
    else:
        return values[0]


cdef int abs_max_ints(int i, int j):
    return j if abs(j) > abs(i) else i


cdef int abs_max_list(list values):
    return max(values, key=lambda x: abs(x))


cpdef str count_range(int i_number):
    ''' Provides a range of given integer :
    123 would give '100 - 1,000' '''
    cdef int i_len
    cdef float d_number
    cdef str s_number, s_0, s_round
    cdef bint b_negative = i_number < 0
    i_number = abs(i_number)
    d_number = i_number - 0.0001
    
    if i_number == 0:
        return '0'
    else:
        s_number = str(d_number)
        i_len=len(s_number)
        s_0 = '0' * (int(log10(d_number)))
        
        if b_negative:
            s_round = '-{:,} - -{:,}'.format(int('1' + s_0), int('10' + s_0))
        else:
            s_round = '{:,} - {:,}'.format(int('1' + s_0), int('10' + s_0))
    return s_round


# much code can be improved by using a datastructe.
cdef dict e_ORD_SUFFIXES = {
    1: 'st', 2: 'nd', 3: 'rd'
}


cpdef str ordinal(int i_num):
    # I'm checking for 10-20 because those are the digits that
    # don't follow the normal counting scheme.
    cdef str suffix
    cdef bint b_negative = i_num < 0
    i_num = abs(i_num)
    
    if 10 <= i_num % 100 <= 20:
        suffix = 'th'
    else:
        # the second parameter is a default.
        suffix = e_ORD_SUFFIXES.get(i_num % 10, 'th')
    if b_negative:
        suffix = '-{}{}'.format("{:,}".format(i_num), suffix)
    else:
        suffix = '{}{}'.format("{:,}".format(i_num), suffix)
    return suffix


cdef dict e_suffixes = {
    3 : 'k',
    6 : 'm',
    9 : 'b',
    12 : 't',
    # 15 : 'p',
    # 18 : 'e',
}
cdef int i_max
cdef str s_max
i_max, s_max = max(e_suffixes.items(), key=itemgetter(0))


cpdef str round_count(int i_number):
    '''returns short notation of numbers above 1000
     like 32234 would give 32k'''
    cdef int i_round, i_len
    cdef str s_rounded, s_count
    cdef bint b_negative = i_number < 0
    i_number = abs(i_number)
    
    if i_number < 1000:
        s_count = '{:,}'.format(i_number)
    else:
        i_round = len(str(i_number)) - 2
        s_rounded = str(round(i_number, -i_round))
        i_len = len(str(i_number))

        if i_len == 3:
            if s_rounded[1] != '0':
                s_count = '{}.{}{}'.format(s_rounded[0], s_rounded[1], e_suffixes[i_len])
            else:
                s_count = '{}{}'.format(s_rounded[0], e_suffixes[i_len])

        elif i_len in e_suffixes:
            s_count = '{}{}'.format(s_rounded[:3], e_suffixes[i_len - 3])

        elif i_len + 1 in e_suffixes:
            s_count = '{}{}'.format(s_rounded[:2],e_suffixes[i_len - 2])

        elif i_len + 2 in e_suffixes:
            if s_rounded[1] != '0':
                s_count = '{}.{}{}'.format(s_rounded[0], s_rounded[1], e_suffixes[i_len - 1])
            else:
                s_count = '{}{}'.format(s_rounded[0], e_suffixes[i_len - 1])

        elif i_len -1 == i_max:
            s_count = '{:,}{}'.format(round(i_number, -i_round) / 10**i_max, s_max)
        else:
            s_count = '{:,.0f}{}'.format(round(i_number, -i_round) / 10**i_max, s_max)
    
    if b_negative:
        return "-{}".format(s_count)
    else:
        return s_count
