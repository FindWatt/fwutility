import multiprocessing
from multiprocessing import Pool
from functools import partial

__all__ = ["cores_to_use", "run_parallel"]


def cores_to_use():
    cpu_count = multiprocessing.cpu_count()
    if cpu_count >= 8:
        return cpu_count - 2
    elif cpu_count > 2:
        return cpu_count - 1
    else:
        return cpu_count


def run_parallel(iterable, func, static_kwargs=None, n_cores=2):
    """
    @description: maps a function to an iterable, using several cpu cores.
    @arg iterable: {iterable} The iterable of arguments to pass through the function
    @arg func: {function} The function to map to the iterable
    @arg static_kwargs: {dict} additional arguments to pass to func, in the form of keyword args.
    Defaults to no arguments passed.
    @arg n_cores: {int} The number of CPU cores to use.
    """
    static_kwargs = {} if static_kwargs is None else static_kwargs
    # Functions passed to map can only take a single variable argument,
    # so we freeze the other args, wrapping the function with a partial function
    partial_f = partial(func, **static_kwargs)

    p = Pool(n_cores)
    results = p.map(partial_f, iterable)
    p.close()
    p.join()
    return results
