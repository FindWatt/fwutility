'''
This module is for generic functions
'''
import os, sys, time
import heapq
from itertools import islice
import requests
import psutil
try: #reduce is not included directly in Python3
    from functools import reduce
except:
    pass

__all__ = [
    "python_version", "secondsToStr",
    "process_time", "process_memory", "process_mem",
    "memory_usage",
    "filter_kwargs",
    "multiindex", "take",
    "nth_largest", "nth_largest_value_index",
    "profileRuntime", "get_public_ip", "round_to",
    "printerror", "obtain_instance_type",
    "check_key", "check_is_key_value", "check_key_is_value",
    "check_key_is_object", "check_key_is_type", "check_key_is_in_list",
    "flatten", "get_dict_values",]


def python_version(b_full_version=False):
    if b_full_version:
        return sys.version_info[0] + sys.version_info[1] / 10 + sys.version_info[2] / 100
    else:
        return sys.version_info[0]


# from http://stackoverflow.com/questions/1557571/how-to-get-time-of-a-python-program-execution
def secondsToStr(t):
    ''' convert seconds into a human-readable time string '''
    return "%d:%02d:%02d.%03d" % \
        reduce(lambda ll,b : divmod(ll[0],b) + ll[1:],
            [(t*1000,),1000,60,60])


def process_time():
    ''' return the process time for debug printing '''
    return secondsToStr(time.process_time())


def process_memory():
    ''' return the processes unique memory (uss) for debug printing '''
    return "{:,}kb".format(round(process_mem('uss')))


def process_mem(measurement='uss'):
    ''' returns the process memory info in Kib.
        Default is unique process memory (uss) '''
    current_process = psutil.Process(os.getpid())
    mem = current_process.memory_full_info()
    mem_kib = {name: byt / 1024 for name, byt in mem._asdict().items()}
    if measurement:
        return mem_kib.get(measurement)
    else:
        return mem_kib


def memory_usage(variable):
    """ Sum the memory usage of nested variables """
    if isinstance(variable, (list, tuple, set)):
        return sys.getsizeof(variable) + sum([memory_usage(var) for var in variable])
    elif isinstance(variable, dict):
        return sys.getsizeof(variable) + sum([memory_usage(var) for var in variable.values()])
    else:
        return sys.getsizeof(variable)


def filter_kwargs(kwargs, c_available_keyword_args):
    ''' Filter a kwarg dictionary down to a list of valid keywords.
        This is important for cython processes functions since they
        can't used **kwargs
        Arguments:
            kwargs: {dict} of keyword arguments
            c_available_keyword_args: {set} of allowed keyword names for a particular function
        Returns:
            {dict} of valid kwargs
    '''
    if not kwargs or not c_available_keyword_args: return {}
    return {key: val for key, val in kwargs.items() if key in c_available_keyword_args}


def multiindex(item, a_list):
    ''' Get all the indexes of a particular item '''
    return [i for i, x in enumerate(a_list) if x == item]


def take(n, iterable):
    ''' Return first n items of the iterable as a list '''
    return list(islice(iterable, n))


def nth_largest(n, iter):
    ''' return the nlargest value from an iterable '''
    try:
        return heapq.nlargest(n, iter)[-1]
    except IndexError:
        return None


def nth_largest_value_index(n, iter):
    ''' return a tuple of the nlargest value and its index from an iterable '''
    try:
        nth = heapq.nlargest(n, iter)[-1]
        return (nth, iter.index(nth))
    except IndexError:
        return None


def profileRuntime(f):
    """This is a decorator. It measures the run time of a given function"""
    def timed(*args, **kwargs):
        t0 = time.time()
        result = f(*args, **kwargs)
        t1 = time.time()
        print("="*60)
        print("That  took {} seconds".format(t1-t0))
        print("="*60)
    return timed


def get_public_ip():
    '''Obtains the machine's public IP.'''
    r = requests.get("https://api.ipify.org/")
    if r.status_code is 200: return r.text


# from http://stackoverflow.com/questions/4265546/python-round-to-nearest-05
def round_to(n, precision):
    correction = 0.5 if n >= 0 else -0.5
    return int(n / precision + correction) * precision


def printerror(s_message):
    '''Print an error sys.stderr if possible, otherwise to normal command line out'''
    try:
        #print(s_message, file=sys.stderr)
        sys.stderr.write(s_message)
    except:
        print(s_message)


def check_key(e_dict, key):
    ''' Check if key is in dictionary and is not empty, then return Value/None. '''
    if key in e_dict and e_dict[key]:
        return e_dict[key]
    else:
        return None


def check_is_key_value(e_dict, key):
    ''' Check if key is in dictionary and is not empty, then return True/False. '''
    return key in e_dict and e_dict[key]


def check_key_is_value(e_dict, key, value):
    ''' Check if key is in dictionary and equals value. '''
    return key in e_dict and e_dict[key] == value


def check_key_is_object(e_dict, key, o_object):
    ''' Check if key is in dictionary and is a certain object. '''
    return key in e_dict and e_dict[key] is o_object


def check_key_is_type(e_dict, key, var_type):
    ''' Check if key is in dictionary and is a certain variable type. '''
    return key in e_dict and isinstance(e_dict[key], var_type)


def check_key_is_in_list(e_dict, key, a_list):
    ''' Check if key is in dictionary and the value is in a list. '''
    return key in e_dict and e_dict[key] in a_list


def obtain_instance_type():
    """ Tries to obtain the instance type of the EC2 it's running on """
    try:
        instance_type = subprocess.Popen(
            ["ec2-metadata", "-t"],
            stdout=subprocess.PIPE
        ).communicate()[0]
        instance_type = instance_type.replace("instance-type", "").replace("\n", "")
    except:
        instance_type = ""

    return instance_type


def flatten(l):
    """Flatten a nested list"""
    elements = []
    for el in l:
        if type(el) is list:
            for sub in flatten(el):
                elements.append(sub)
        else:
            elements.append(el)
    return elements


def get_dict_values(dictionary):
    """ Gets all the values of a dictionary and all the dictionaries
    nested inside """
    if isinstance(dictionary, set):
        dict_values = list(dictionary)
    elif isinstance(dictionary, dict):
        dict_values = []
        for el in dictionary.values():
            if isinstance(el, set):
                for el2 in el:
                    dict_values.append(el2)
            elif isinstance(el, dict):
                for el2 in get_dict_values(el):
                    dict_values.append(el2)
            else:
                dict_values.append(el)
    return dict_values
