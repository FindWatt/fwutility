# ### General purpose utilities ### #

These are an assorted collection of utility functions that don't fit into the broader categories of the FwText, FwFreq, FwFValues, FwNLP, and FwFile projects.

Example usage:

```
#!python

import FwUtility
from FwUtility.misc import take
from FwUtility import parallel_processing

my_ip = get_public_ip()

l = ["one", "one", "two", "three", "four", "four"]

assert take(3, l) == ["one", "one", "two"]

assert FwUtility.multiindex("four", l) == [4, 5]

mp_cores_to_process_with = parallel_processing.cores_to_use()
```