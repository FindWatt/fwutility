﻿import pytest
import math, re
import numpy as np
import FwUtility


class TestAbsMin():
    def test_blank(self):
        assert FwUtility.abs_min() is None
        
    def test_single_val(self):
        assert FwUtility.abs_min(5) == 5
        
    def test_two_vals(self):
        assert FwUtility.abs_min(5, -10) == 5
        
    def test_three_vals(self):
        assert FwUtility.abs_min(5, -10, 2) == 2
        
    def test_list(self):
        assert FwUtility.abs_min([5, -10, 2]) == 2

class TestAbsMax():
    def test_blank(self):
        assert FwUtility.abs_max() is None
        
    def test_single_val(self):
        assert FwUtility.abs_max(5) == 5
        
    def test_two_vals(self):
        assert FwUtility.abs_max(5, -10) == -10
        
    def test_three_vals(self):
        assert FwUtility.abs_max(5, -10, 15) == 15
        
    def test_list(self):
        assert FwUtility.abs_max([5, -10, 2]) == -10


class TestCountRange():
    def test_negative_101(self):
        assert "-100 - -1,000"  == FwUtility.count_range(-101)
    def test_negative_100(self):
        assert "-10 - -100"  == FwUtility.count_range(-100)
    def test_negative_10(self):
        assert "-1 - -10"  == FwUtility.count_range(-10)
    def test_negative_5(self):
        assert "-1 - -10"  == FwUtility.count_range(-5)
    def test_zero(self):
        assert "0"  == FwUtility.count_range(0)
    def test_1(self):
        assert "1 - 10"  == FwUtility.count_range(1)
    def test_9(self):
        assert "1 - 10"  == FwUtility.count_range(9)
    def test_10(self):
        assert "1 - 10"  == FwUtility.count_range(10)
    def test_11(self):
        assert "10 - 100"  == FwUtility.count_range(11)
    def test_99(self):
        assert "10 - 100"  == FwUtility.count_range(99)
    def test_100(self):
        assert "10 - 100"  == FwUtility.count_range(100)
    def test_101(self):
        assert "100 - 1,000"  == FwUtility.count_range(101)
    def test_1000(self):
        assert "100 - 1,000"  == FwUtility.count_range(1000)
    def test_1001(self):
        assert "1,000 - 10,000"  == FwUtility.count_range(1001)
    def test_10000(self):
        assert "10,000 - 100,000"  == FwUtility.count_range(10000)


class TestOrdinal():
    def test_negative_100(self):
        assert "-100th"  == FwUtility.ordinal(-100)
    def test_negative_10(self):
        assert "-10th"  == FwUtility.ordinal(-10)
    def test_negative_5(self):
        assert "-1st"  == FwUtility.ordinal(-1)
    def test_zero(self):
        assert "0th"  == FwUtility.ordinal(0)
    def test_1(self):
        assert "1st"  == FwUtility.ordinal(1)
    def test_2(self):
        assert "2nd"  == FwUtility.ordinal(2)
    def test_3(self):
        assert "3rd"  == FwUtility.ordinal(3)
    def test_4(self):
        assert "4th"  == FwUtility.ordinal(4)
    def test_9(self):
        assert "9th"  == FwUtility.ordinal(9)
    def test_10(self):
        assert "10th"  == FwUtility.ordinal(10)
    def test_11(self):
        assert "11th"  == FwUtility.ordinal(11)
    def test_99(self):
        assert "99th"  == FwUtility.ordinal(99)
    def test_100(self):
        assert "100th"  == FwUtility.ordinal(100)
    def test_101(self):
        assert "101st"  == FwUtility.ordinal(101)
    def test_1000(self):
        assert "1,000th"  == FwUtility.ordinal(1000)
    def test_1001(self):
        assert "1,001st"  == FwUtility.ordinal(1001)
    def test_10000(self):
        assert "10,000th"  == FwUtility.ordinal(10000)


class TestRoundCount():
    def test_negative_1000(self):
        assert "-1k"  == FwUtility.round_count(-1000)
    def test_negative_100(self):
        assert "-100"  == FwUtility.round_count(-100)
    def test_negative_10(self):
        assert "-10"  == FwUtility.round_count(-10)
    def test_negative_1(self):
        assert "-1"  == FwUtility.round_count(-1)
    def test_zero(self):
        assert "0"  == FwUtility.round_count(0)
    def test_1(self):
        assert "1"  == FwUtility.round_count(1)
    def test_10(self):
        assert "10"  == FwUtility.round_count(10)
    def test_11(self):
        assert "11"  == FwUtility.round_count(11)
    def test_100(self):
        assert "100"  == FwUtility.round_count(100)
    def test_101(self):
        assert "101"  == FwUtility.round_count(101)
    def test_999(self):
        assert "999"  == FwUtility.round_count(999)
    def test_1000(self):
        assert "1k"  == FwUtility.round_count(1000)
    def test_1001(self):
        assert "1k"  == FwUtility.round_count(1001)
    def test_1001(self):
        assert "1k"  == FwUtility.round_count(1001)
    def test_1600(self):
        assert "1.6k"  == FwUtility.round_count(1600)
    def test_1999(self):
        assert "2k"  == FwUtility.round_count(1999)
    def test_10000(self):
        assert "10k"  == FwUtility.round_count(10000)
