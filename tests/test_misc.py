import pytest
import math, re
import numpy as np
from collections import OrderedDict
import FwUtility

class TestVersion():
    def test_version(self):
        s_partial = FwUtility.python_version(b_full_version=False)
        s_full = FwUtility.python_version(b_full_version=True)
        print(s_partial)
        assert s_partial
        print(s_full)
        assert s_full


class TestSecondsToStr():
    def test_seconds_to_string(self):
        assert FwUtility.secondsToStr(5) == "0:00:05.000"


class TestProcessTime():
    def test_process_time(self):
        s_time = FwUtility.process_time()
        assert s_time
        assert isinstance(s_time, str)
    
    def test_process_time_format(self):
        s_time = FwUtility.process_time()
        assert re.search(r"\d+:\d\d:\d\d.\d+", s_time)


class TestProcessMemory():
    def test_process_memory(self):
        s_mem = FwUtility.process_memory()
        assert s_mem
        assert isinstance(s_mem, str)
    
    def test_process_memory_format(self):
        s_mem = FwUtility.process_memory()
        print(s_mem)
        assert re.search(r"\d{1,3}(,\d\d\d)*kb", s_mem)


class TestProcessMem():
    def test_process_mem_default(self):
        d_mem = FwUtility.process_mem()
        print(d_mem)
        assert d_mem > 0
        assert isinstance(d_mem, (float, int))
    
    def test_process_mem_components(self):
        e_mem = FwUtility.process_mem(None)
        print(e_mem)
        assert isinstance(e_mem, dict)
        assert 'uss' in e_mem


class TestFilterKwargs():
    kwargs = {"a": 1, "b": 2, "c": 3}
    keywords = {"a", "d"}
    
    def test_blank_dict(self):
        result = FwUtility.filter_kwargs(None, self.keywords)
        assert isinstance(result, dict) and len(result) == 0
    
    def test_blank_keywords(self):
        result = FwUtility.filter_kwargs(self.kwargs, None)
        assert isinstance(result, dict) and len(result) == 0
    
    def test_normal(self):
        result = FwUtility.filter_kwargs(self.kwargs, self.keywords)
        assert isinstance(result, dict) and len(result) == 1


class TestMultiIndex():
    def test_blank_item(self):
        assert FwUtility.multiindex(None, ["item"]) == []
        
    def test_blank_list(self):
        assert FwUtility.multiindex("item", []) == []
        
    def test_number(self):
        assert FwUtility.multiindex(2, [1, 2, 3, 4, 3, 2, 1]) == [1, 5]
        
    def test_strings(self):
        assert FwUtility.multiindex("test", ["test", "this", "function", "test", "out"]) == [0, 3]
        
    def test_non_match(self):
        assert FwUtility.multiindex("no-match", ["test", "this", "function", "test", "out"]) == []


class TestTake():
    def test_blank(self):
        assert FwUtility.take(5,[]) == []
    def test_list(self):
        assert FwUtility.take(2,["test","this","function","out"]) == ["test","this"]
    def test_dict(self):
        e_test = OrderedDict([("test",0), ("this",1), ("function",2), ("out",3)])
        assert FwUtility.take(2,e_test) == ['test', 'this']
        assert FwUtility.take(2,e_test.items()) == [("test",0), ("this",1)]


class TestNthLargest():
    def test_blank(self):
        assert FwUtility.nth_largest(1,[]) == None
    def test_list(self):
        a_test = [5,1,4,10,2,6,8,3,9,7]
        assert FwUtility.nth_largest(1,a_test) == 10
        assert FwUtility.nth_largest(2,a_test) == 9
        assert FwUtility.nth_largest(3,a_test) == 8
        assert FwUtility.nth_largest(4,a_test) == 7
        assert FwUtility.nth_largest(5,a_test) == 6
        assert FwUtility.nth_largest(6,a_test) == 5
        assert FwUtility.nth_largest(7,a_test) == 4
        assert FwUtility.nth_largest(8,a_test) == 3
        assert FwUtility.nth_largest(9,a_test) == 2
        assert FwUtility.nth_largest(10,a_test) == 1
    def test_dict(self):
        at_test = [("test",0), ("this",1), ("function",2), ("out",3)]
        a_keys,a_values = zip(*at_test)
        assert FwUtility.nth_largest(1,a_values) == 3


class TestNthLargestValueIndex():
    def test_blank(self):
        assert FwUtility.nth_largest_value_index(1,[]) == None
    def test_list(self):
        a_test = [5,1,4,10,2,6,8,3,9,7]
        assert FwUtility.nth_largest_value_index(1,a_test) == (10, 3)
    def test_dict(self):
        at_test = [("test",0), ("this",3), ("function",1), ("out",2)]
        a_keys,a_values = zip(*at_test)
        assert FwUtility.nth_largest_value_index(1,a_values) == (3, 1)


class TestGetPublicIP():
    def test_ip(self):
        s_ip = FwUtility.get_public_ip()
        assert re.search(r"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}", s_ip)


class TestRuntime():
    @FwUtility.profileRuntime
    def test_rangeloop(self):
        for i in range(1000): pass

class TestRoundTo():
    def test_zero(self):
        assert FwUtility.round_to(0,1) == 0
        assert FwUtility.round_to(0,0.1) == 0
    def test_int(self):
        assert FwUtility.round_to(1,1) == 1
        assert FwUtility.round_to(1,0.1) == 1
    def test_float(self):
        assert math.isclose(FwUtility.round_to(1.23456,1), 1)
        assert math.isclose(FwUtility.round_to(1.23456,0.1), 1.2)
        assert math.isclose(FwUtility.round_to(1.23456,0.01), 1.23)
        assert math.isclose(FwUtility.round_to(1.23456,0.001), 1.235)
        assert math.isclose(FwUtility.round_to(1.23456,0.0001), 1.2346)
        assert math.isclose(FwUtility.round_to(1.23456,0.00001), 1.23456)
        

class printerror():
    def test_printerror(self):
        FwUtility.printerror("printerror worked successfully in unit test")


class TestDictFunctions():
    testlist = ['object']
    testdict = {"test":0, "this":"1", "function":testlist, "out":None}
    
    def test_check_key(self):
        assert FwUtility.check_key(self.testdict, "this") == "1"
        assert FwUtility.check_key(self.testdict, "TEST") is None
    
    def test_check_is_key_value(self):
        assert FwUtility.check_is_key_value(self.testdict, "this") == "1"
        assert FwUtility.check_is_key_value(self.testdict, "out") is None
        assert FwUtility.check_is_key_value(self.testdict, "TEST") == False
    
    def test_check_key_is_value(self):
        assert FwUtility.check_key_is_value(self.testdict, "test", 0) == True
        assert FwUtility.check_key_is_value(self.testdict, "this", 2) == False
        assert FwUtility.check_key_is_value(self.testdict, "TEST", 1) == False
    
    def test_check_key_is_object(self):
        assert FwUtility.check_key_is_object(self.testdict, "function", self.testlist) == True
        assert FwUtility.check_key_is_object(self.testdict, "function", ['object']) == False
    
    def test_check_key_is_type(self):
        assert FwUtility.check_key_is_type(self.testdict, "test", int) == True
        assert FwUtility.check_key_is_type(self.testdict, "this", str) == True
        assert FwUtility.check_key_is_type(self.testdict, "function", list) == True
    
    def test_check_key_is_in_list(self):
        a_test = [0,1]
        assert FwUtility.check_key_is_in_list(self.testdict, "test", a_test) == True
        assert FwUtility.check_key_is_in_list(self.testdict, "this", a_test) == False


class TestInstanceType():
    def test_non_ec2(self):
        ec2_type = FwUtility.obtain_instance_type()
        assert isinstance(ec2_type, str)

class TestFlatten():
    def test_blank(self):
        assert FwUtility.flatten([]) == []
        assert FwUtility.flatten([[],[],[]]) == []
    def test_lists(self):
        testlist = [[1,2,3],['a','b','c']]
        assert FwUtility.flatten(testlist) == [1,2,3,'a','b','c']


class TestGetDictValues():
    def test_blank(self):
        assert FwUtility.get_dict_values({}) == []
        assert FwUtility.get_dict_values(set([])) == []
        assert FwUtility.get_dict_values({'a':{},'b':set([])}) == []
    def test_dict(self):
        assert set(FwUtility.get_dict_values({1,2,3,'a','b','c'})) == set([1,2,3,'a','b','c'])
        assert set(FwUtility.get_dict_values({1:{1:1,2:2,3:3},2:{1:'a',2:'b',3:'c'},3:set('d')})) == set([1,2,3,'a','b','c','d'])
